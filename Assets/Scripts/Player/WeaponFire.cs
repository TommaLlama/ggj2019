﻿using System.Collections;
using UnityEngine;

public class WeaponFire : MonoBehaviour {
    
    public int fireRate;
    public GameObject projectilePrefab;
    public Transform spawnPoint;
    private float nextFire;

    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(projectilePrefab, spawnPoint.position, spawnPoint.rotation);
        }
    }

}