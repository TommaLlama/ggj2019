﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SheepCounter : MonoBehaviour
{
    public Text sheeps;

    public int sheepNum;

    //public string inText;

	// Use this for initialization
	void Start ()
    {
        //sheeps = GetComponent<Text>();
        sheepNum = 0000;
        sheeps.text = "Sheep : " + sheepNum;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyUp(KeyCode.UpArrow))
        {
            AddScore(10);
        }
	}

    public void AddScore(int newScoreValue)
    {
        sheepNum += newScoreValue;
        UpdateScore();
    }

    public void UpdateScore()
    {
        sheeps.text = "Sheep : " + sheepNum;
    }
}