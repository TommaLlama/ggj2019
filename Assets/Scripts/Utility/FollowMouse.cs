﻿using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    //Need a way to adjust speed
    public float rotateSpeed;

    void Update()
    {
        FaceMouse();
    }

    void FaceMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        //Using 2D..
        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        transform.up = direction * Time.deltaTime * rotateSpeed;

        print(mousePosition);
    }

}