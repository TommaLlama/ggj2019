﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    private Rigidbody2D myRigidbody;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        ForceMode2D forceMode = ForceMode2D.Impulse; 
        myRigidbody.AddForce(transform.up * speed * Time.deltaTime, forceMode);
    }
}
