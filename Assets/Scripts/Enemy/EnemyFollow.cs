﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow: MonoBehaviour{

    public float moveSpeed;
    private Transform target;
    [SerializeField]
    private float followDistance;
    //Only follow when you're not attacking and within range
    private bool follow;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update()
    {
        Follow(); 
        //Attack(target);       
    }

    bool IsWithinRange(Transform target)
    {
        float distance = Vector2.Distance(target.position, transform.position);
        if(distance < followDistance)
        {
            return true;
        }
        else
        {
            return false; 
        }
    }

    bool IsWithinAttackRange(Transform target)
    {
        float distance = Vector2.Distance(target.position, transform.position);
        if(distance < 1f)
        {
            return true;
        }
        else
        {
            return false; 
        }
    }

    void Follow()
    {
        if(IsWithinRange(target))
        {
            Debug.Log(string.Format("I'm following {0}", target.name));
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        }
        else
        {
            Debug.Log("Target escaped");
        }
    }

    void Attack(Transform target)
    {
        Debug.Log(string.Format("I'm attacking {0}"));
    }
}